const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _description: String,
    _role: String,
    _functionality: String,
    _benefit: String,
    _priority: String,
    _size: String,
    _criteria_of_acceptance: String,
    _context: String,
    _event: { type: [String], default: undefined },
    _result: { type: [String], default: undefined },
    _card_value: Number,
    _column_id: { type: String, required: true },
    _board_id: { type: String, required: true }
});

class Storie {

    constructor(
        description,
        role,
        functionality,
        benefit,
        priority,
        size,
        criteriaOfAcceptance,
        context,
        event,
        result,
        cardValue,
        columnId,
        boardId
    ) {
        this._description = description;
        this._role = role;
        this._functionality = functionality;
        this._benefit = benefit;
        this._priority = priority;
        this._size = size;
        this._criteria_of_acceptance = criteriaOfAcceptance;
        this._context = context;
        this._event = event;
        this._result = result;
        this._card_value = cardValue;
        this._column_id = columnId;
        this._board_id = boardId;
    }

    get priority() {
        return this._priority;
    }

    set priority(v) {
        this._priority = v;
    }

    get description() {
        return this._description;
    }

    set description(v) {
        this._description = v;
    }

    get role() {
        return this._role;
    }

    set role(v) {
        this._role = v;
    }

    get functionality() {
        return this._functionality;
    }

    set functionality(v) {
        this._functionality = v;
    }

    get benefit() {
        return this._benefit;
    }

    set benefit(v) {
        this._benefit = v;
    }

    get size() {
        return this._size;
    }

    set size(v) {
        this._size = v;
    }

    get criteriaOfAcceptance() {
        return this._criteria_of_acceptance;
    }

    set criteriaOfAcceptance(v) {
        this._criteria_of_acceptance = v;
    }

    get context() {
        return this._context;
    }

    set context(v) {
        this._context = v;
    }

    get event() {
        return this._event;
    }

    set event(v) {
        this._event = v;
    }

    get result() {
        return this._result;
    }

    set result(v) {
        this._result = v;
    }

    get cardValue() {
        return this._card_value;
    }

    set cardValue(v) {
        this._card_value = v;
    }

    get columnId() {
        return this._column_id;
    }

    set columnId(v) {
        this._column_id = v;
    }

    get boardId() {
        return this._board_id;
    }

    set boardId(v) {
        this._board_id = v;
    }
}

schema.plugin(mongoosePaginate)
schema.loadClass(Storie);
module.exports = mongoose.model('Storie', schema);