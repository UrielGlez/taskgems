const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _project_name: String,
    _project_request_date: Date,
    _project_start_date: Date,
    _description: String,
    _scrum_master: String,
    _product_owner: String,
    _members: {type: [String], default: undefined},
    _background_url: String,
    _is_close: Boolean
});

class Board {

    constructor(projectName, projectRequestDate, projectStartDate,
      description, scrumMaster, productOwner, members, backgroundUrl, isClose) {
        this._project_name = projectName;
        this._project_request_date = projectRequestDate;
        this._project_start_date = projectStartDate;
        this._description = description;
        this._scrum_master = scrumMaster;
        this._product_owner = productOwner;
        this._members = members;
        this._background_url = backgroundUrl;
        this._is_close = isClose;
    }

    get productOwner() {
        return this._product_owner;
    }

    set productOwner(v) {
        this._product_owner = v;
    }

    get scrumMaster() {
        return this._scrum_master;
    }

    set scrumMaster(v) {
        this._scrum_master = v;
    }

    get description() {
        return this._description;
    }

    set description(v) {
        this._description = v;
    }

    get projectStartDate() {
        return this._project_start_date;
    }

    set projectStartDate(v) {
        this._project_start_date = v;
    }

    get projectRequestDate() {
        return this._project_request_date;
    }

    set projectRequestDate(v) {
        this._project_request_date = v;
    }

    get projectName() {
        return this._project_name;
    }

    set projectName(v) {
        this._project_name = v;
    }

    get backgroundUrl() {
        return this._background_url;
    }

    set backgroundUrl(v) {
        this._background_url = v;
    }

    get isClose() {
        return this._is_close;
    }

    set isClose(v) {
        this._is_close = v;
    }
}


schema.plugin(mongoosePaginate)
schema.loadClass(Board);
module.exports = mongoose.model('Board', schema);
