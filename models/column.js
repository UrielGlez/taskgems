const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _name: String,
    _board_id: String
});

class Column {

    constructor(name, boardId) {
        this._name = name;
        this._board_id = boardId;
    }

    get name() {
        return this._name;
    }

    set name(v) {
        this._name = v;
    }

    get boardId() {
        return this._board_id;
    }

    set boardId(v) {
        this._board_id = v;
    }
}


schema.plugin(mongoosePaginate)
schema.loadClass(Column);
module.exports = mongoose.model('Column', schema);