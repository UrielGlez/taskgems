const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _email: String,
    _first_name: String,
    _last_name: String,
    _birthday: Date,
    _curp: String,
    _rfc: String,
    _address: Object,
    _abilities: Object,
    _boards: { type: [String], default: undefined },
    _password: String,
    _salt: String
});

class User {

    constructor(email, name, lastName, birthday, curp, rfc, address, abilities, boards, password, salt) {
        this._email = email;
        this._first_name = name;
        this._last_name = lastName;
        this._birthday = birthday;
        this._curp = curp;
        this._rfc = rfc;
        this._address = address;
        this._abilities = abilities;
        this._boards = boards;
        this._password = password;
        this._salt = salt;
    }

    get abilities() {
        return this._abilities;
    }

    set abilities(v) {
        this._abilities = v;
    }

    get address() {
        return this._address;
    }

    set address(v) {
        this._address = v;
    }

    get rfc() {
        return this._rfc;
    }

    set rfc(v) {
        this._rfc = v;
    }

    get curp() {
        return this._curp;
    }

    set curp(v) {
        this._curp = v;
    }

    get birthday() {
        return this._birthday;
    }

    set birthday(v) {
        this._birthday = v;
    }

    get salt() {
        return this._salt;
    }

    set salt(v) {
        this._salt = v;
    }

    get email() {
        return this._email;
    }

    set email(v) {
        this._email = v;
    }

    get name() {
        return this._first_name;
    }

    set name(v) {
        this._first_name = v;
    }

    get lastName() {
        return this._last_name;
    }

    set lastName(v) {
        this._last_name = v;
    }

    get password() {
        return this._password;
    }

    set password(v) {
        this._password = v;
    }
}


schema.plugin(mongoosePaginate)
schema.loadClass(User);
module.exports = mongoose.model('User', schema);