const Board = require("../models/board");
const User = require("../models/user");

function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;
  let deleted = req.params.deleted ? req.params.deleted : false;
  Board.paginate({ 
    $or: [
      {_scrum_master: req.user.id},
      {_product_owner: req.user.id},
      {_members: req.user.id}
    ], _is_close: deleted
  }, { page: page, limit: 5 })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.board.loaded"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.loaded"),
        obj: error,
      })
    );
}

async function addUsers(req, res, next) {
  const boardId = req.params.id;
  const userId = req.params.id2;

  try {
    const boardPromise = await Board.update({ "_id": boardId }, { $push: { _members: userId } });
    const userPromise = await User.update({ "_id": userId }, { $push: { _boards: boardId } });
    return res.status(200).json({
      "message": res.__("ok.user.added.to.board"),
      objs: [boardPromise, userPromise],
    });
  } catch (err) {
    return res.status(500).json({
      "message": res.__("err.user.not.found"),
      obj: error,
    });
  }

  /*Board.update({ "_id": boardId }, { $push: { _members: userId } })
    .then((obj) => {
      User.update({ "_id": boardId }, { $push: { _members: userId } }).then((obj2) =>
        res.status(200).json({
          "message": res.__("ok.user.added.to.board"),
          objs: obj2,
        })
      ).catch(
        res.status(500).json({
          "message": res.__("err.user.not.found"),
          obj: error,
        })
      );
    })
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.found"),
        obj: error,
      })
    );*/

}

function create(req, res, next) {
  let projectName = req.body._project_name;
  let projectRequestDate = req.body._project_request_date;
  let projectStartDate = req.body._project_start_date;
  let description = req.body._description;
  let scrumMaster = req.body._scrum_master;
  let productOwner = req.body._product_owner;
  let backgroundUrl = req.body._background_url;
  let isClose = req.body._is_close;

  let board = new Board({
    _project_name: projectName,
    _project_request_date: projectRequestDate,
    _project_start_date: projectStartDate,
    _description: description,
    _scrum_master: scrumMaster,
    _product_owner: productOwner,
    _background_url: backgroundUrl,
    _is_close: isClose
  });

  board.save().then(obj => res.status(200).json({
    "message": res.__("ok.board.created"),
    objs: obj
  })).catch(error => res.status(500).json({
    "message": res.__("err.board.not.storaged"),
    objs: error
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Board.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.board.showed"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.found"),
        obj: error,
      })
    );
}

function replace(req, res, next) {
  let id = req.params.id;
  let projectName = req.body._project_name ? req.body._project_name : "";
  let projectRequestDate = req.body._project_request_date ? req.body._project_request_date : "";
  let projectStartDate = req.body._project_start_date ? req.body._project_start_date : "";
  let description = req.body._description ? req.body._description : "";
  let scrumMaster = req.body._scrum_master ? req.body._scrum_master : "";
  let productOwner = req.body._product_owner ? req.body._product_owner : "";
  let backgroundUrl = req.body._background_url ? req.body._background_url : "";
  let isClose = req.body._is_close ? req.body._is_close : false;

  let board = new Object({
    _project_name: projectName,
    _project_request_date: projectRequestDate,
    _project_start_date: projectStartDate,
    _description: description,
    _scrum_master: scrumMaster,
    _product_owner: productOwner,
    _background_url: backgroundUrl,
    _is_close: isClose
  });

  Board.findOneAndReplace({ _id: id }, board)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.board.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.found"),
        obj: error,
      })
    );
}

function update(req, res, next) {
  let id = req.params.id;
  let projectName = req.body._project_name;
  let projectRequestDate = req.body._project_request_date;
  let projectStartDate = req.body._project_start_date;
  let description = req.body._description;
  let scrumMaster = req.body._scrum_master;
  let productOwner = req.body._product_owner;
  let backgroundUrl = req.body._background_url;
  let isClose = req.body._is_close;

  let board = new Object();
  if (projectName) {
    board._project_name = projectName;
  }
  if (projectRequestDate) {
    board._project_request_date = projectRequestDate;
  }
  if (projectStartDate) {
    board._project_start_date = projectStartDate;
  }
  if (description) {
    board._description = description;
  }
  if (scrumMaster) {
    board._scrum_master = scrumMaster;
  }
  if (productOwner) {
    board._product_owner = productOwner;
  }
  if (backgroundUrl) {
    board._background_url = backgroundUrl;
  }
  if (isClose) {
    board._is_close = isClose;
  }

  Board.findOneAndUpdate({ _id: id }, board)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.board.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.found"),
        obj: error,
      })
    );
}
function destroy(req, res, next) {
  const id = req.params.id;
  Board.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.board.deleted"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.board.not.found"),
        obj: error,
      })
    );
}
module.exports = {
  list,
  addUsers,
  index,
  create,
  replace,
  update,
  destroy,
};