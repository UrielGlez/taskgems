const { mongo } = require("mongoose");
const User = require("../models/user");

function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;

  User.paginate({}, {page: page, limit: 10})
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.user.loaded"), 
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.user.not.loaded"),
        obj: error,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  User.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.user.showed"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.user.not.found"),
        obj: error,
      })
    );
}

function replace(req, res, next) {
  let id = req.params.id;

  let email = req.body.email ? req.body.email : "";
  let name = req.body.firstName ? req.body.firstName : "";
  let lastName = req.body.lastName ? req.body.lastName : "";
  let birthday = req.body.birthday ? req.body.birthday : "";
  let curp = req.body.curp ? req.body.curp : "";
  let rfc = req.body.rfc ? req.body.rfc : "";
  let address = req.body.address ? req.body.address : "";
  let abilities = req.body.abilities ? req.body.abilities : "";
  let password = req.body.password ? req.body.password : "";

  let user = new Object({
    _email: email,
    _first_name : name,
    _last_name: lastName,
    _birthday: birthday,
    _curp: curp,
    _rfc: rfc,
    _address: address,
    _abilities: abilities,
    _password: password,
  });

  User.findOneAndReplace({ _id: id }, user)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.user.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.user.not.found"),
        obj: error,
      })
    );
}

function update(req, res, next) {
  let id = req.params.id;
  let email = req.body.email;
  let name = req.body.firstName;
  let lastName = req.body.lastName;
  let birthday = req.body.birthday;
  let curp = req.body.curp;
  let rfc = req.body.rfc;
  let address = req.body.address;
  let abilities = req.body.abilities;
  let password = req.body.password;

  let user = new Object();

  if (email) {
    user._email = email;
  }
  if (name) {
    user._first_name = name;
  }
  if (lastName) {
    user._last_name = lastName;
  }
  if (password) {
    user._password = password;
  }
  if (birthday) {
    user._birthday = birthday;
  }
  if (curp) {
    user._curp = curp;
  }
  if (rfc) {
    user._rfc = rfc;
  }
  if (address) {
    user._address = address;
  }
  if (abilities) {
    user._abilities = abilities;
  }

  User.findOneAndUpdate({ _id: id }, user)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.user.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.user.not.found"),
        obj: error,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  
  User.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.user.deleted"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.user.not.found"),
        obj: error,
      })
    );
}

module.exports = {
  list,
  index,
  replace,
  update,
  destroy,
};