const express = require("express");
const { Logger } = require("log4js");
const Storie = require("../models/storie");

function list(req, res, next) {
  let board = req.params.board;
  Storie.paginate({_board_id: board}, { page: 1, limit: 100 })
    .then((obj) =>
      res.status(200).json({
        message:  res.__("ok.storie.loaded"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message:  res.__("ok.storie.not.loaded"),
        obj: error,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;
  Storie.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message:  res.__("ok.storie.showed"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message:  res.__("ok.storie.not.found"),
        obj: error,
      })
    );
}

function create(req, res, next) {
  let description = req.body.description;
  let role = req.body.role;
  let functionality = req.body.functionality;
  let benefit = req.body.benefit;
  let priority = req.body.priority;
  let size = req.body.size;
  let criteriaOfAcceptance = req.body.criteriaOfAcceptance;
  let context = req.body.context;
  let event = req.body.event;
  let result = req.body.result;
  let cardValue = req.body.cardValue;
  let columnId = req.body.columnId;
  let boardId = req.body.boardId;

  let storie = new Storie({
    _description: description,
    _role: role,
    _functionality: functionality,
    _benefit: benefit,
    _priority: priority,
    _size: size,
    _criteria_of_acceptance: criteriaOfAcceptance,
    _context: context,
    _event: event,
    _result: result,
    _card_value: cardValue,
    _column_id: columnId,
    _board_id: boardId
  });

  storie.save().then(obj => res.status(200).json({
    message: res.__("ok.storie.created"),
    objs: obj
  })).catch(error => res.status(500).json({
    message: res.__("ok.storie.not.storaged"),
    objs: error
  }));
}

function replace(req, res, next) {
  let id = req.params.id;
  let description = req.body.description ? req.body.description : "";
  let role = req.body.role ? req.body.role : "";
  let functionality = req.body.functionality ? req.body.functionality : "";
  let benefit = req.body.benefit ? req.body.benefit : "";
  let priority = req.body.priority ? req.body.priority : "";
  let size = req.body.size ? req.body.size : "";
  let criteriaOfAcceptance = req.body.criteriaOfAcceptance ? req.body.criteriaOfAcceptance : "";
  let context = req.body.context ? req.body.context : "";
  let event = req.body.event ? req.body.event : "";
  let result = req.body.result ? req.body.result : "";
  let cardValue = req.body.cardValue ? req.body.cardValue : "";
  let columnId = req.body.columnId ? req.body.columnId : "";
  let boardId = req.body.boardId ? req.body.boardId : "";

  let storie = new Object({
    _description: description,
    _role: role,
    _functionality: functionality,
    _benefit: benefit,
    _priority: priority,
    _size: size,
    _criteria_of_acceptance: criteriaOfAcceptance,
    _context: context,
    _event: event,
    _result: result,
    _card_value: cardValue,
    _column_id: columnId,
    _board_id: boardId
  });

  Storie.findOneAndReplace({ _id: id }, storie)
    .then((obj) =>
      res.status(200).json({
        message: res.__("ok.storie.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: res.__("ok.storie.not.found"),
        obj: error,
      })
    );
}

function update(req, res, next) {
  const id = req.params.id;
  let description = req.body.description;
  let role = req.body.role;
  let functionality = req.body.functionality;
  let benefit = req.body.benefit;
  let priority = req.body.priority;
  let size = req.body.size;
  let criteriaOfAcceptance = req.body.criteriaOfAcceptance;
  let context = req.body.context;
  let event = req.body.event;
  let result = req.body.result;
  let cardValue = req.body.cardValue;
  let columnId = req.body.columnId;
  let boardId = req.body.boardId;

  let storie = new Object();
  if (description) {
    storie._description = description;
  }
  if (role) {
    storie._role = role;
  }
  if (functionality) {
    storie._functionality = functionality;
  }
  if (benefit) {
    storie._benefit = benefit;
  }
  if (priority) {
    storie._priority = priority;
  }
  if (size) {
    storie._size = size;
  }
  if (criteriaOfAcceptance) {
    storie._criteria_of_acceptance = criteriaOfAcceptance;
  }
  if (context) {
    storie._context = context;
  }
  if (event) {
    storie._event = event;
  }
  if (result) {
    storie._result = result;
  }
  if (cardValue) {
    storie._card_value = cardValue;
  }
  if (columnId) {
    storie._column_id = columnId;
  }
  if (boardId) {
    storie._board_id = boardId;
  }

  Storie.findOneAndUpdate({ _id: id }, storie)
    .then((obj) =>
      res.status(200).json({
        message: res.__("ok.storie.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: res.__("ok.storie.not.found"),
        obj: error,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Storie.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: res.__("ok.storie.deleted"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: res.__("ok.storie.not.found"),
        obj: error,
      })
    );
}

module.exports = {
  list,
  index,
  create,
  replace,
  update,
  destroy,
};