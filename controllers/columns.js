const Column = require("../models/column");

function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;

  Column.paginate({}, {page: page, limit: 10})
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.loaded"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.loaded"),
        obj: error,
      })
    );
}

function listFilter(req, res, next) {
  const boardId = req.params.id;
  let page = req.params.page ? req.params.page : 1;

  Column.paginate({ _board_id: boardId }, {page: page, limit: 10})
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.loaded"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.loaded"),
        obj: error,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  Column.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.showed"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.found"),
        obj: error,
      })
    );
}

function create(req, res, next) {
  let name = req.body.name;
  let boardId = req.body.boardId;

  let column = new Column({
      _name: name,
      _board_id: boardId,
  });

  column.save().then(obj => res.status(200).json({
      "message": res.__("ok.column.created"),
      objs: obj
  })).catch(error => res.status(500).json({
      "message": res.__("err.column.not.storaged"),
      objs: error
  }));
}

function replace(req, res, next) {
  let id = req.params.id;

  let name = req.body.name ? req.body.name : "";
  let boardId = "";
  

  let column = new Object({
    _name : name,
    _board_id: boardId
  });

  Column.findOneAndReplace({ _id: id }, column)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.found"),
        obj: error,
      })
    );
}

function update(req, res, next) {
  let id = req.params.id;
  let name = req.body.name;
  

  let column = new Object();

  if (name) {
    column._name = name;
  }

  Column.findOneAndUpdate({ _id: id }, column)
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.updated"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.found"),
        obj: error,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  
  Column.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": res.__("ok.column.deleted"),
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": res.__("err.column.not.found"),
        obj: error,
      })
    );
}

module.exports = {
  list,
  listFilter,
  index,
  create,
  replace,
  update,
  destroy,
};