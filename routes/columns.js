const express = require('express');
const controller = require('../controllers/columns');

const router = express.Router();

// CRUD => http methods

router.post('/', controller.create);

router.get('/:page?', controller.list);

router.get('/filter/:id', controller.listFilter);

router.get('/index/:id', controller.index);

router.patch('/', controller.replace);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;