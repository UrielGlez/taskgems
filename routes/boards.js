const express = require('express');
const controller = require('../controllers/boards');

const router = express.Router();

// CRUD => http methods
router.post('/', controller.create);

router.post('/add-to-board/:id/:id2', controller.addUsers);

router.get('/:page?/:deleted?', controller.list);

router.get('/index/by/id/yes/:id', controller.index);

router.patch('/', controller.replace);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;
